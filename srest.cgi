#!/usr/local/bin/perl
#This script is the main handler for the simple rest server.
#This script is invoked for every HTTP request with a URI of
# http://server/rest/app/user/myobject. For any request to be
# processed the app and user must be registered on the server.
# The script takes different actions depending on the request
# method:
#
# POST - The script creates a file whose name is the
#        last part of the URI (in the above example
#        the file will be called myobject). The file
#        will be stored in a directory that is unique
#        for the application, user and any other 
#        additional parts of the URI. The current 
#        implementation would store the above URI
#        in the path /restdata/app/user/myobject.
#        Responses:
#        200 OK - If the file existed and has been rewritten
#                 with the new data.
#        201 Created - The file or path did not exist, but
#                      it was succesfully created and the 
#                      data was succesfully stored.
#        500 Internal Server Error - The server encountered
#                                    an error and was unable
#                                    to fulfill request.
#
#
# GET - The script returns the data for a previously
#       stored URI. 
#       Responses:
#       200 OK - The content of the object was returned
#                succesfully.
#       404 Not Found - The requested object can not be
#                       found.
# 

# Imported modules
use CGI qw/:standard/;           # Handle requests and print headers
use Fcntl ':flock';              # Import LOCK_* constants
use File::Path qw(make_path);    # To mkdir -p the path for storing data
use Encode;                      # To use utf8 encoding for json
use File::HomeDir;               # to get HOME dir for config
use File::Spec::Functions;       # Mostly for catfile
use Switch;                      # For switch()

$_SREST_VERSION = 0.54;

##############################################################
#######               BEGIN CONFIG SETUP               #######
##############################################################
#First thing is to load the configuration,
#user and application permissions
BEGIN
{
   #Default config values
   $CFG{'cfgdir'}     = catdir(File::HomeDir->my_home,
                        ".config",
                        "srest");
   $CFG{'cfgfile'}    = 'srest.cfg';
   $CFG{'logfile'}    = 'srest.log';
   $CFG{'maxlogsize'} = 200000;
   $CFG{'debuglevel'} = 2;
   $CFG{'baseurl'}    = "/srest";
   $CFG{'basedir'}    = catdir(File::HomeDir->my_home,".srestdata");

   #Load config and permissions from config file
   my $cfgfile  = catfile( $CFG{'cfgdir'}, $CFG{'cfgfile'} );
   my ($sec,$lval,$rval) = ("","","");

   open CFG,"<:utf8",$cfgfile
      or die "Unable to open config: '$cfgfile'";
   while(<CFG>)
   {
      chomp;
      #Parse sections
      $sec = "core",next          if /\[core\]/;
      $sec = "users",next         if /\[users\]/;
      $sec = "applications",next  if /\[applications\]/;

      #Ignore blank lines
      next if /^#.*$|^\s*$/;

      #Make sure lvals and rvals are alphanumberic
      $lval=$1, $rval=$2 if /^\s*(\w+)\s*=\s*((\w+,?)+)\s*/;
      
      #Handle core config
      $CFG{$lval} = $rval if $sec eq "core";

      #Users and user permissions
      if ($sec eq "users")
      {
         die "User must have GET or POST or DELETE permission" 
            if ($rval !~ /(GET,?|POST,?|DELETE,?)+/);
         $USERS{$lval} = $rval;
      } 

      #App permissions
      if ($sec eq "applications")
      {
         die "App must have GET or POST or DELETE permission" 
            if ($rval !~ /(GET,?|POST,?|DELETE,?)+/);
         $APPS{$lval} = $rval;
      }
   }
   close CFG;
}

##############################################################
#######              BEGIN LOG SETUP                   #######
##############################################################
#Open  LOG file at the beginning  of script
BEGIN
{
   #Truncate and restart log if file size > maxlogsize,
   #otherwise append
   my $file = catfile($CFG{'cfgdir'},$CFG{'logfile'});
   my $mode = '>>:utf8';
   my $fsize = -s "$file";

   $mode = '>:utf8' if $fsize > $CFG{'maxlogsize'};
   open $log,$mode,$file or die "Can't open log $file";
}


#Close LOG file at the end  of script
END
{
   my $file = catfile($CFG{'cfgdir'},$CFG{'logfile'});
   close $log or die "Can't close log $file";
}
##############################################################
#######              END LOG SETUP                     #######
##############################################################

$DBGLEVEL=$CFG{'debuglevel'};
sub Log;

sub loadConfig
{
   #Load config and permissions from config file
   my $cfgfile  = catfile( $CFG{'cfgdir'}, $CFG{'cfgfile'} );
   my ($sec,$lval,$rval) = ("","","");

   open CFG,"<:utf8",$cfgfile
      or die "Unable to open config: '$cfgfile'";
   while(<CFG>)
   {
      chomp;
      #Parse sections
      $sec = "core",next          if /\[core\]/;
      $sec = "users",next         if /\[users\]/;
      $sec = "applications",next  if /\[applications\]/;

      #Ignore blank lines
      next if /^#.*$|^\s*$/;

      #Make sure lvals and rvals are alphanumberic
      $lval=$1, $rval=$2 if /^\s*(\w+)\s*=\s*((\w+,?)+)\s*/;

      #Handle core config
      $CFG{$lval} = $rval if $sec eq "core";

      #Users and user permissions
      if ($sec eq "users")
      {
         die "User must have GET or POST or DELETE permission" 
            if ($rval !~ /(GET,?|POST,?|DELETE,?)+/);
         $USERS{$lval} = $rval;
      } 

      #App permissions
      if ($sec eq "applications")
      {
         die "App must have GET or POST or DELETE permission" 
            if ($rval !~ /(GET,?|POST,?|DELETE,?)+/);
         $APPS{$lval} = $rval;
      }
   }
   close CFG;
}

sub dbg
{
   my $msg = shift;
   
   $DBG .= "\t\t       " . $msg . "\n" if $msg;
   return $DBG unless $msg;
   return 1;
}

sub getHTTPCode
{
   my $code = shift;
   my %CODES=( '200' => "200 OK",
               '201' => "201 Created",
               '404' => "404 Not Found",
               '500' => "500 Internal Server Error",
               '501' => "501 Not Implmented" );

   return $CODES{$code};
}

sub Log
{
   my $msg = shift;
   my $file = catfile($CFG{'cfgdir'},$CFG{'logfile'});

   my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
   $year = 1900 + $year;
   $mon = sprintf("%02d",$mon + 1);
   $mday = sprintf("%02d",$mday);
   $hour = sprintf("%02d",$hour);
   $min = sprintf("%02d",$min);
   $sec = sprintf("%02d",$sec);


   flock($log, LOCK_EX) or die "Could not lock '$file' - $!";

   print $log "$year-$mon-$mday $hour:$min:$sec -- $msg\n";
   print $log dbg();

   flock($log, LOCK_UN) or die "Could not unlock '$file' - $!";
}

##########################################################################
####                          PRINT DEBUG PAGE                        ####
##########################################################################
sub LogCGIInfo
{
   # Print the CGI form variables
   foreach (keys %vars) 
   {
      dbg("$_ = \'$vars{$_}\'");
   }

   # Print all environmnet variable available to the 
   # CGI script
   foreach (keys %ENV) 
   {
      dbg("$_ = \'$ENV{$_}\'");
   }
}


##########################################################################
####                        HTML ERROR PAGE                           ####
##########################################################################
# Die, insert HTML error page
# in the javascript object
sub HTTPDie 
{
   my ($status,$msg,$title)= @_ ;
   $title= getHTTPCode($status) unless $title;
   $msg = "No more details" unless $msg;

   #Send error page to client
   printHeader($status,"text/html");

   print <<EOF ;
<html>
<head>
<title>REST server error</title>
</head>
<body>
<h1>$title</h1>
<li>$msg</li>
</body>
</html>
EOF
   #Log error in server's log
   Log("***** HTTPDie exit: $status - '$msg'");
   exit ;
}

##########################################################################
####                        Authorize app and user                    ####
##########################################################################
sub Authorize
{
   #Authorize application and user
   #Each user and app is authorized for 
   #the methods listed in the configuration file

   #Parameters: url and method
   local( $_ )= shift;
   my $method = shift;

   #Get app and user from url
   my ( $app, $user )= (split(m!/!,$_,5))[2,3];

   #Die if user or app are not verified

   my @userlist = keys %USERS;
   my @applist = keys %APPS;
   HTTPDie(404,"SECURITY: App '$app' or user '$user' do not exist")
      unless ($app ~~ @applist) && ($user ~~ @userlist);

   HTTPDie(404,"SECURITY: $method denied for app '$app' and user '$user'")
      unless (  $APPS{$app} =~ /$method/ ) && ( $USERS{$user} =~ /$method/ );

   dbg("Authorized app  '$app' for $method",2);
   dbg("Authorized user '$user' for $method",2);
   
   /^$CFG{'baseurl'}/ or HTTPDie(404,"SECURITY: URL '$_' does not contain '$CFG{'baseurl'}'");
}


##########################################################################
####                        Get Storage directory                     ####
##########################################################################
sub GetDataFile
{
   my $BASEDIR = $CFG{'basedir'};
   local($_) = shift;
   
   #Get file (BASEDIR/app/user/...)  
   my $file =  "$BASEDIR/" . substr($_,6);

   dbg("Data file = '$file'",1);
   
   /^$CFG{'baseurl'}/ && return $file or HTTPDie(404,"Invalid url: '$_'")
}

sub retrieveData
{
   my $file = shift;
   Log("Retrieving data from '$file'");
  
   #Read data from file, ask for a shared lock to make
   #sure writers to the file are finished 
   open my $in,'<:utf8', $file or HTTPDie(404,"Unable to open '$file' for reading");
   flock($in, LOCK_SH) or HTTPDie(500,"Unable to lock '$file' for reading - $!");

   chomp(my $timestamp = <$in>);
   my $contents = do { local $/;  <$in> };
   Log("Timestamp $timestamp ms. \n\t\t\t" . length($contents) . " bytes from '$file' read");

   close $in or HTTPDie(404,"Unable to read from '$file' after open"); 

   #Print HTTP header and data
   printHeader(200,"application/json",$timestamp);
   
   print $contents;

   return 200;
}


sub storeData
{
   my $file = shift;
   my $data = shift;
   my $httpstatus = 200;  #Everything OK by default
   my $timestamp = $ENV{'HTTP_X_REST_TS'};

   #Create dir if necessary
   chomp(my $dir = `dirname "$file"`);
   make_path("$dir") and dbg("Created directory: '$dir'",1) unless -d "$dir";

   #Write data to file
   $httpstatus = 201 unless -f "$file";
   Log("Storing '" . substr($data,0,12) . (length($data) > 12 ? "...":"") . "' in $file");
   open my $out,'>:utf8', $file or HTTPDie(500,"Unable to open '$file' for output");
   flock($out, LOCK_EX) or HTTPDie(500,"Could not lock '$file' for writing - $!");
   print $out "$timestamp\n";
   print $out $data;
   close $out or HTTPDie(500,"Unable to write data to '$file'");

   #Everything was fine, send 200 OK or 201 Created
   printHeader($httpstatus,"application/json");

   return $httpstatus;
}

sub deleteData
{
   my $file = shift;
   my $data = shift;

   HTTPDie("404","File not found: '$file'") if not -f $file;
   HTTPDie("500","Unable to delete file: '$file'") unless unlink($file);

   Log("Deleted file '$file'");
   printHeader("200");
   return 200;
}


sub printHeader
{
   my $status = shift;
   my $type = shift;
   my $timestamp = shift;

   $type = "text/html" if not $type;
   $status = getHTTPCode($status);

   Log("Response: $status");

   print $cgi->header( -type => $type,
                       -charset => 'utf-8',
                       -status => $status,
                       '-X-rest-ts' => $timestamp) if $timestamp;

   print $cgi->header( -type => $type,
                       -charset => 'utf-8',
                       -status => $status)            unless $timestamp;
}

##########################################################################
##########################################################################
####                                MAIN                              ####
##########################################################################
##########################################################################

#print "APPS=",%APPS,"Keys=",keys %APPS,"\n";
#print "USERS=",%USERS,"Keys=",keys %USERS,"\n";

$url = $ENV{"REDIRECT_URL"} or die "No URI given.";
$method = $ENV{"REQUEST_METHOD"} or die "No method found.";

Log "Received $method request";


# Get variables from CGI environment
$cgi = CGI->new;  
%vars = $cgi->Vars();

#Verify user and app, exit if they are not allowed
Authorize($url,$method);

#Get file where data is stored
$path = GetDataFile($url);

LogCGIInfo() if $DBGLEVEL > 5;

#Process request
switch ($method)
{
  case "GET"    {  $status = retrieveData($path)                 }
  case "POST"   {  $status = storeData($path,$vars{'POSTDATA'})  }
  case "PUT"    {  $status = storeData($path,$vars{'POSTDATA'})  }
  case "DELETE" {  $status = deleteData($path)                   }
  else          {  HTTPDie("501","Method '$_' not supported")    }
}

Log "***** $method request finished.";
exit;

