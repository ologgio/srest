$sr.tlog = function()
{
   var res = $("#srest-debug").text()
   if ( /%(s|o|d)/.test(arguments[0]) )
      res = res +  
         sprintf.apply(null, Array.prototype.slice.call(arguments, 0))
   else
   {
      var i
      var out = []
      for(i=0; i < arguments.length; i++)
         out.push(arguments[i] + " ")

      res = res + out.join("")
   }
   $("#srest-debug").text(res + "\n")
   //console.log(c)
}

$sr.printres = function (tname,res)
{
   var failed = ' FAILED !!!!!!!!!!!!!'
   var passed = ' PASSED'
   var style = (res) ? 'style="color: blue"':'style="background-color: red"'
   $sr.tlog('<h3 ' + style + '">' + tname + 
         ((res) ? passed:failed) + "</h3>" )
}

$sr.filltest = function ()
{
   var o = { 
              title: "Title from Object",
              data: "data from object",
              array: [ "element 0", "element 1" ]
           }
   var o1 = [ { 
                   title: "Title from Object#0",
                   data: "data from object#0",
                   array: [ "#0 - element 0", "#0 - element 1" ]
                },
                {
                   title: "Title from Object#1",
                   data: "data from object#1",
                   array: [ "#1 - element 0", "#1 - element 1" ]
                } 
            ] 
            
   var goodhtml = "\n\
            <h1>Fill Test - Title from Object</h1>\n\
            <ul>\n\
            <li>data is data from object</li>\n\
            <li>array[0] = element 0</li>\n\
            <li>array[1] = element 1</li>\n\
            <li>o1=[0] = Title from Object#0,data from object#0 <br>\n\
                   [1]Title from Object#1,data from object#1\n\
            </li>\n\
         </ul>"

   $sr.fill("#fill-test",o)
   console.log("fill-test newhtml=%s",$('#fill-test').html())
   $sr.fill("#fill-test",o1)
   console.log("fill-test newhtml=%s",$('#fill-test').html())
   console.log("fill-test expectedhtml=%s",goodhtml)
   return ( goodhtml === $('#fill-test').html() )
}

$sr.test = function (testName)
{
   var finalres = true
   $sr.tlog("<h2>" + testName + "</h2>")

   dotest = function(method, test)
   {
      return function(a,b,c) 
      {
         var res = test(a,b,c)
         if (res) 
            $sr.tlog('<li style="color: blue;">' + method + " - passed</strong>")
         else
            $sr.tlog('<li style="color: red; font-weight:bold;"><strong>' + method + " - failed</strong></li>")

         finalres = finalres && res
      }
   }

   $sr.post('http://www.newsfromgod.com/srest/medaid/ologgio/meds',
         '[{ "name": "night1" },{ "name": "day2" }]',
         dotest("POST",function () { return true  } ), 
         dotest("POST",function () { return false } ))

   $sr.get('http://www.newsfromgod.com/srest/medaid/ologgio/meds',
         dotest("GET",function (a,b,c) { return a[0].name == "night1" && a[1].name == "day2" } ),
         dotest("GET",function () { return false  } ))

   $sr.post('http://www.newsfromgod.com/srest/medaid/ologgio/meds',
         '[{ "name": "night3" },{ "name": "day4" }]',
         dotest("POST update",function () { return true  } ), 
         dotest("POST update",function () { return false } ))

   $sr.get('http://www.newsfromgod.com/srest/medaid/ologgio/meds',
         dotest("GET after POST update",function (a,b,c) { return a[0].name == "night3" && a[1].name == "day4" } ),
         dotest("GET after POST update",function () { return false  } ))

   $sr.del('http://www.newsfromgod.com/srest/medaid/ologgio/meds',
         dotest("DELETE",function () { return true  } ),
         dotest("DELETE",function () { return false } ))

   $sr.get('http://www.newsfromgod.com/srest/medaid/ologgio/meds',
         dotest("GET after DELETE",function () { return false  } ),
         dotest("GET after DELETE",function () { return true  } ))
   
   $sr.get('http://www.newsfromgod.com/srest/medaid/ologgio1/meds',
         dotest("GET with wrong user",function () { return false  } ),
         dotest("GET with wrong user",function () { return true  } ))

   $sr.get('http://www.newsfromgod.com/srest/medaid1/ologgio/meds',
         dotest("GET with wrong app",function () { return false  } ),
         dotest("GET with wrong app",function () { return true  } ))

   $sr.get('http://www.newsfromgod.com/srest1/medaid/ologgio/meds',
         dotest("GET with wrong URI",function () { return false  } ),
         dotest("GET with wrong URI",function () { return true  } ))

   $sr.printres(testName,finalres)

   return finalres
}

$sr.cfg["async"] = false  //We need to run tests synchronously
                           //to properly test
$sr.cfg["usenetwork"] = true
$sr.test("TEST with network")

$sr.cfg["usenetwork"] = false
$sr.test("TEST offline (no network) ")

res = $sr.filltest()
$sr.printres("Template fill test",res)

$sr.cfg.defaults()

