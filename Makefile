PROD=yes
DIST=dist
PRJ=srest
VERSIONFILE=$(PRJ)-version
VERSION:=$(shell tail -1 $(VERSIONFILE))

WWWOUT=$(DIST)/www
BASEURL:=ftp://ftp.newsfromgod.com/public_html/newsfromgod
UPWWWURL=$(BASEURL)/srest/

#File that will be copied in to dist 
SRC= .htaccess  			\
	  README.md  			\
	  srest.cfg.example	\
	  srest.cgi	 			\
	  srest.html

#JS files that will be concatenated in srest.js
JSSRC= tools/zepto.min.js \
		 tools/sprintf.js \
		 srest.js 

JSTEST= test/srest.test.js

JSOUT= $(WWWOUT)/$(PRJ).js
ifeq ($(strip $(PROD)),yes)
   JSPRODOUT= $(WWWOUT)/$(PRJ).min.js
else
	JSPRODOUT=
endif

#Build distribution, copying needed files to $WWWOUT,
#and updating everything
.PHONY: dist
dist: $(SRC) $(JSSRC) $(JSOUT) $(JSTEST) $(JSPRODOUT)
	cp $(SRC) $(WWWOUT)

#Build main project javascript, concatenating all js into one file
$(JSOUT): $(JSSRC) $(JSTEST) 
	@[[ -d $(WWWOUT) ]] || mkdir -p $(WWWOUT)
	@if [[ "$(PROD)" = "no" ]]; then          \
		echo "Building test version.";    \
	   echo cat $(JSSRC) $(JSTEST) \> $(JSOUT); \
	   cat $(JSSRC) $(JSTEST) > $(JSOUT);       \
	else                           			     \
		echo "Building production version.";     \
	   echo cat $(JSSRC) \> $(JSOUT);    	     \
      cat $(JSSRC) > $(JSOUT);    			     \
	fi 

ifeq ($(strip $(PROD)),yes)
$(JSPRODOUT): $(JSOUT)
	#"Compile" javascript with Google Closure Compiler
	@echo "Minimizing script with Google Closure Compiler" && \
	echo "...please wait, it takes a long time."          && \
	java -jar tools/compiler.jar --js "$(JSOUT)" --js_output_file "$(JSPRODOUT)" 2>compiler.log && \
	echo "Finished making '$(JSPRODOUT)'" && \
	echo "Compilation done. Log is in 'compiler.log'"; 
endif


#Update version number in srest.cgi
$(PRJ).cgi: $(VERSIONFILE)
	sed -i 's/^\s*\$$_SREST_VERSION\s*=.*/$$_SREST_VERSION = $(VERSION);/' $@

#Update version number in srest.js
$(PRJ).js: $(VERSIONFILE)
	sed -i "s/^\s*\_SREST_VERSION\s*=.*/_SREST_VERSION = $(VERSION)/" $@

#These files don't need any specifig rule to be built
#this is mainly so Make doesn't complain about not having rules
test/srest-test.js:
tools/zepto.min.js:
srest.htm:

#upload files to server
upload: dist
	@while [[ -z "$$UPUSER" ]]; do \
		read -er -p "Ftp upload server User: " UPUSER;\
	done && \
	while [ -z "$$UPPWD" ]; do \
		read -er -p "Ftp upload server password: " UPPWD; \
	done && shopt -s dotglob && \
	   cd "$(WWWOUT)" && \
		   FILES="`ls -m * | sed -e 's/, /,/g' | tr -d '\n'`" && \
			printf "\nUploading $$FILES:\n" && \
	      curl -T \{"$$FILES"\} $(UPWWWURL) -u "$$UPUSER:$$UPPWD";

clean:
	rm -rf "$(WWWOUT)/"

#cd $(BBOUT)/OTAInstall && curl -T \{"`ls -m *.cod *.jad | sed -e 's/, /,/g' | tr -d '\n'`"\} -u $(UPUSER) $(UPCODURL) -T $(PRJDIR)/$(WWWDIR)/index.html $(UPINDEXURL)

