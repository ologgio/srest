_SREST_VERSION = 0.54

//Add Date.now() if we don't have it
if (!Date.now) 
{  
   Date.now = function now() { return +(new Date) }
}  

srest = {}
srest.priv = {}
srest["localStorage"] = true
srest["online"] = false
srest.cfg = {}
srest.def = {}
srest.def.cfg = {}
srest.def.cfg["usenetwork"] = true
srest.def.cfg["async"] = true
srest.def.cfg.allowednets = {}
srest.def.cfg.allowednets["3GPP"]  = true
srest.def.cfg.allowednets["CDMA"]  = true
srest.def.cfg.allowednets["iDEN"]  = true
srest.def.cfg.allowednets["Wi-Fi"] = true
srest.def.cfg.allowednets["other"] = true

$sr = srest;

srest.cfg.defaults = function()
{
   $.extend(srest.cfg,             srest.def.cfg)
   $.extend(srest.cfg.allowednets, srest.def.cfg.allowednets)
}

srest.cfg.defaults()

function encode_utf8( s )
{
   return unescape( encodeURIComponent( s ) );
}

function decode_utf8( s )
{
   return decodeURIComponent( escape( s ) );
}

srest.localget = function(url)
{
   try
   {
      var data = JSON.parse(localStorage[url])
      var ts   = localStorage[url+"__ts__"]
      $sr.tlog("localGET '",data,"',ts=",ts)
      if (data != undefined)
         return { data: data, ts: ts };
      else
         return null;
   }
   catch(e)
   {
      srest["localStorage"] = false
      return null;
   }
}

srest.usenetwork = function()
{
   var currnet="other";
   var net,res=false;

   if (typeof blackberry != "undefined")
      currnet = blackberry.network.toLowerCase()

   for(net in $sr.cfg.allowednets)
   {
      net = net.toLowerCase()
      res = res || (currnet.indexOf(net) === -1)
   }

   $sr.tlog("<li> currnet="+currnet+" cfg[usenework]"+srest.cfg["usenetwork"]+" allowed[cur]="+ srest.cfg.allowednets[currnet] + " res=" + res)

   return srest.cfg["usenetwork"] && res
}

srest.netup = function()
{
   $sr.tlog("navigator.Online=" + navigator.onLine)
   $sr.tlog("srest[online]=" + srest["online"])

   return navigator.onLine || srest["online"]
}

srest.errorAlert = function (type,msg)
{
   alert('Error posting data to server!\n'
         + new Date() 
         + 'Error type: ' + type
         + 'Error msg:  ' + msg)
}

srest.netget = function(url,successfunc,errorfunc,locdata,lGetOK)
{
   try
   {
      $.ajax({
               type: 'GET',
               url: url,
               dataType: 'json',
               timeout: 300,
               context: $('body'),
               async: srest.cfg["async"],
               global: false,
               success: function(netdata,status,xhr)
               { 
                  var serverts;
                  (serverts = xhr.getResponseHeader("X-rest-ts")) || (serverts = -1);
                  $sr.tlog("GET netpost serverts =",serverts) 

                  // Update server's data if local data is newer.
                  // If the update is not successful the next srest.get will
                  // try again
                  if (lGetOK && (locdata.ts > serverts ) )
                  {
                     $sr.tlog("Updating server data with ",locdata.data,
                                   "localts=",locdata.ts,
                                   "serverts=",serverts)
                     srest.netpost(url,locdata.data,locdata.ts)
                  }

                  // only send the network data to the user if it is new data
                  // also update local cache with the new data
                  if (!locdata || (serverts > locdata.ts))
                  {
                     $sr.tlog("Updating local data with ",netdata,
                                   "localts=", (locdata) ? locdata.ts:"null",
                                   "serverts=",serverts)
                     srest.localpost(url,netdata,serverts) 
                     successfunc && successfunc(netdata,status,xhr,"network")
                  }

                  // If serverts===localts then the data is in sync
                  // so we don't need to do anything
               },
               error: function(xhr, type, msg)
               {
                  if (errorfunc) errorfunc(xhr,type, msg)
                  else
                     srest.errorAlert("getting",type,msg);
               }
            })
   }
   catch(e)
   {
      if (typeof e == "XMLHttpRequestException")
         errorfunc && error(null,e.name, e.message)
   }
}

// successfunc(data,status,xhr,origin)
// errorfunc(xhr,type,msg)
// netwait: do not call successfunc after getting data from local cache, but
//          wait to see if there is newer information from the network.
srest.get = function(url,successfunc,errorfunc, netwait)
{
   var lGetOK = false;
   var locdata;

   // Obtain data from local cache, call errorfunc if there is a problem
   locdata = srest.localget(url)
   locdata && (lGetOK = true)
   
   // Call sucessfunc if we have data from cache
   // if netwait is false or not sepcified then 
   // successfunc will be called a second time if we
   // get newer data from the server, notice the second
   // parameter indicates origin of data: local or network.
   //
   // if netwait is true then successfunc is NOT called
   // when we have data from local cache, but we wait until
   // ajax calls the successfunc.
   lGetOK && !netwait && successfunc && successfunc(locdata.data,"200",null,"local")

   if (srest.usenetwork() && srest.netup())
   {   
      srest.netget(url,successfunc,errorfunc,locdata,lGetOK)
   }
   else
   {
      if (!lGetOK)
      {
         if (errorfunc) errorfunc(null,"localdataerror","Local data error")
         else
            srest.errorAlert("getting","localdataerror","Error retrieving local data");
      }
   }
}

srest.localpost = function(url,data,ts)
{
   try
   {
      localStorage[url] = data
      localStorage[url+"__ts__"] = ts
      $sr.tlog("localPOST '",data,"',ts=",ts)
      srest["localStorage"] = true
      return true
   }
   catch(e)
   {
      srest["localStorage"] = false
      return false;
   }
}

srest.netpost = function(url,data,timestamp,successfunc,errorfunc)
{
   try
   {
   $.ajax({
            type: 'POST',
            url: url,
            data: encode_utf8(data),
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            headers: { "X-rest-ts" : timestamp},
            timeout: 300,
            context: $('body'),
            async: srest.cfg["async"],
            global: false,
            success: function(response,status,xhr)
            { 
               successfunc && successfunc(response,status,xhr,"network")
            },
            error: function(xhr, type, msg)
            {
               if (errorfunc) errorfunc(xhr,type,msg)
               else
                  srest.errorAlert("posting",type,msg)
            }
         })
   }
   catch(e)
   {
      if (typeof e == "XMLHttpRequestException")
         errorfunc && error(null,e.name, e.message)
   }
}

srest.post = function(url,data,successfunc,errorfunc)
{
   var lPostOK = false;
   var ts = Date.now();

   lPostOK = srest.localpost(url,data,ts)

   if (srest.usenetwork() && srest.netup())
   {   
      srest.netpost(url,data,ts,successfunc,errorfunc)
   }
   else
   {
      // If we're not using the network call it a success
      // if the local cache worked 
      lPostOK && successfunc
         && successfunc(null,200,null,"local")

      // If we're not using the network call it an error
      // if the local cache fails
      !lPostOK && errorfunc
         && errorfunc(null,"localdataerror","Unable to store local data")
   }
}

srest.localdelete = function(url,errorfunc)
{
   try
   {
      localStorage.removeItem(url)
      srest["localStorage"] = true
      return true
   }
   catch(e)
   {
      $sr.tlog("localDELETE exception",e);
      srest["localStorage"] = false
      return false;
   }
}

srest.netdelete =function(url,successfunc,errorfunc) 
{
   try
   {
      $.ajax({
               type: 'DELETE',
               url: url,
               dataType: 'json',
               timeout: 300,
               context: $('body'),
               async: srest.cfg["async"],
               global: false,
               success: function(data,status,xhr)
               { 
                  successfunc && successfunc(data,status,xhr,"network")
               },
               error: function(xhr, type, msg)
               {
                  if (errorfunc) errorfunc(xhr,type,msg)
                  else
                     srest.errorAlert("deleting",type,msg)
               }
            })
   }
   catch(e)
   {
      if (typeof e == "XMLHttpRequestException")
         errorfunc && error(null,e.name, e.message)
   }
}

srest.del = function(url,successfunc,errorfunc)
{
   var lDeleteOK = false
   lDeleteOK = srest.localdelete(url)
   if (srest.usenetwork() && srest.netup())
   {   
      srest.netdelete(url,successfunc,errorfunc)
   }
   else
   {
      // If we're not using the network call it a success
      // if the local cache worked 
      lDeleteOK && successfunc
         && successfunc(null,200,null,"local")

      // If we're not using the network call it an error
      // if the local cache fails
      !lDeleteOK && errorfunc
         && errorfunc(null,"localdataerror","Unable to delete local data")
   }
}
//Returns the length of the object array
//indicated by the start loop instruction
//in the html template
srest.priv.getArrayLen = function(name,obj) 
{
   // Get name parts (properties)
   var p = name.split("-")
   var len = -1
   
   // If the caller passed a null name
   if (name == null)
      return -1
   // If the loop start was @@[ without a name
   // then the object that fills the template
   // is the array for which we need the length  
   else if (name == "")
      len = ( $.isArray(obj) ) ? obj.length:-1
   // If the loop start is a property
   // e.g. @@[myprop-myarray, then obj.myprop.myarray is the 
   // array for which we need the length
   else
   {
      var o = obj
      var i = 0
      while ( o.hasOwnProperty(p[i]) )
         o = o[p[i++]]
      len = ( $.isArray(o) ) ? o.length:-1
   }
   $sr.tlog("len=%s,name='%s'",len,name)
   return len
}

srest.priv.repeatBlock = function(block,count,start) 
{
   var i = (start) ? start:0
   var res = []

   if ( count <= 0 || start < 0 ) 
      throw "Wrong count or start value"

   //We use a regex for global replacement
   var regex = new RegExp("\\[\\]","g")

   // Repeat the block changing replacing
   // each [] with the number of the iteration
   while(i<count)
      res.push(block.replace(regex,i++))

   res = res.join("")
   $sr.tlog("repeatBlock result=%s",res)

   return res
}

// Get substitution table for the fill function
// subtbl is of the form "@@property" --> value
srest.priv.prepareHTML = function(html,obj) 
{
   // Find array loop, get name of array variable,
   // and repeating block
   var regex = /@@\[((?:\w|-|\[|\])*)((?:.|\n)*?)@@\]/g
   var h = html

   //replace each loop body with the repetition
   //of the block a number of times 
   //equal to the length of the array 
   //specified by the start loop instruction
   h = html.replace(regex,function ( match, aname, block, offset, orig )
   {
      var len = srest.priv.getArrayLen(aname,obj)
      var res = (len>0) ? srest.priv.repeatBlock(block,len):match
      return res
   })

   $sr.tlog("prepareHTML(...) orig=%s\nprepareHTML(...) repl=%s",html,h)
   return h
}

// Get substitution table for the fill function
// subtbl is of the form "@@property" --> value
srest.priv.getsubtbl = function(obj,prefix) 
{
    var subtbl = {};
    var i, p, append;
    for (p in obj) 
    {
        //If we are in a recursive call we'll get a prefix
        append = (prefix) ? prefix + "-":""

        //If property is an array we add each element of the array
        //@@myarray-0 = myarray[0],@@myarray-1 = myarray[1], etc
        if ($.isArray(obj[p])) 
        {
            for (i = 0; i < obj[p].length; i++) 
            {
               subtbl["@@" + append + p + "-" + i] = obj[p][i];
            }
        //If it is another object call the function 
        //recursively
        } else if (typeof obj[p]==="object") {
           $.extend(subtbl,srest.priv.getsubtbl(obj[p],p))
        // If it is a number or string add 
        // the property directly
        // @@myprop = value
        } else  {
           subtbl["@@" + append + p] = obj[p];
        }
    }
    return subtbl
}


// Fill any attribute values or inner text template strings 
// with the values stored in obj.
// For example if the DOM has this: <a href="@@link">My nice link: @@desc</a> and
// values.link=www.mysite.com/mylink, values.desc="NASA Project", then the DOM
// will be changed to <a href="www.mysite.com/mylink">My nicelink: NASA Project</a>
srest.fill = function(selector, obj) 
{
   var p, h;
   var subtbl = srest.priv.getsubtbl(obj)

   // Sort substitions strings with longest first
   var replkeys = function ()
   {
     var keys = [];
     for(var i in subtbl) if (subtbl.hasOwnProperty(i))
     {
       keys.push(i);
     }
     return keys.sort( function(a,b) { return b.length - a.length } )
   }()

   $(selector).html(function(ndx, html) 
   {
      h = html;

      //Unroll array loops in the html ("@@[...@@]" constructs)
      if (h.indexOf("@@[") !== -1)
         h = srest.priv.prepareHTML(h,obj)

      //Replace @@ parameters with the value
      for (p in replkeys)
      if (replkeys.hasOwnProperty(p)) 
      {
         var regex = new RegExp(replkeys[p],"g")
         h = h.replace(replkeys[p], subtbl[replkeys[p]])
         srest.tlog("replaced %s with %s", replkeys[p], subtbl[replkeys[p]])
      }
      //Clean memory as much as we can
      delete subtbl

      return h;
   })
}

// Decompress an LZW-encoded string
srest.lzwDecode = function (s)
{
   var dict = {};
   var data = (s + "").split("");
   var currChar = data[0];
   var oldPhrase = currChar;
   var out = [currChar];
   var code = 256;
   var phrase;
   for (var i=1; i<data.length; i++) {
      var currCode = data[i].charCodeAt(0);
      if (currCode < 256) {
         phrase = data[i];
      }
      else {
         phrase = dict[currCode] ? dict[currCode] : (oldPhrase + currChar);
      }
      out.push(phrase);
      currChar = phrase.charAt(0);
      dict[code] = oldPhrase + currChar;
      code++;
      oldPhrase = phrase;
   }
   return out.join("");
}


// lzw-encode a string
srest.lzwEncode = function (s)
{
   var dict = {};
   var data = (s + "").split("");
   var out = [];
   var currChar;
   var phrase = data[0];
   var code = 256;
   for (var i=1; i<data.length; i++) 
   {
      currChar=data[i];
      if (dict[phrase + currChar] != null) 
      {
         phrase += currChar;
      }
      else 
      {
         out.push(phrase.length > 1 ? dict[phrase] : phrase.charCodeAt(0));
         dict[phrase + currChar] = code;
         code++;
         phrase=currChar;
      }
   }
   out.push(phrase.length > 1 ? dict[phrase] : phrase.charCodeAt(0));
   for (var i=0; i<out.length; i++) 
   {
      out[i] = String.fromCharCode(out[i]);
   }
   return out.join("");
}
