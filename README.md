sRest - Simple Rest server
===============================

sRest is a very simple **rest server** and **client library**. The client
library is a light-weight javascript library and the server consists of a
simple but robust cgi script. sRest is mainly intended for mobile apps. For
now, only json objects are supported.

QUICK START
-----------

An example will speak more than many words. Suppose you want to post this json
object to the server: `{ date: "2029-05-01", msg: "Into the future"}`. This
code will do it:

    :::javascript
    $sr.post('http://www.myserver.com/srest/msgapp/user/msg1', '{ date:
    "2029-05-01", msg: "Into the future}',successfunc,errorfunc)

This code will get it back:

    :::javascript
    //successfunc will be called with the populated javascript object.
    $sr.get('http://www.myserver.com/srest/msgapp/user/msg1',successfunc,errorfunc)
 
That's it. Among other things, no need to write a rest server, no need to worry
about several clients trying to write at the same time. Also,  objects are
cached locally.


No other code is needed besides installing the cgi script on the server and
using the client library within your web app.

Why?
---- 

Maybe you are wondering why don't I just make the ajax call with
jquery or zepto? These are the reasons:

* No need to write a server
* Same server can be used for many apps
* the sRest client library stores objects locally while the network request to
  the server is being processed, giving you a quick response time (and
  notifying you later if needed)
* sRest time-stamps the objects automatically thus notifying you _only if new
  data is available_.
* sRest server implements a locking mechanism so two clients can't write to the
  same object at the same time
* sRest on the server side is very simple, it does not use a database but
  stores objects in individual files
* A simple authentication mechanism allows security on a per user basis or a
  per application basis, allowing the user to allow or deny GET, POST and
  DELETE methods.
* The sRest client provides a quick and easy HTML template feature to fill HTML
  from javascript objects (this reduces the network bandwidth because the
  server does not have to send html tags, etc)


- - -

_Note_: This readme was previewed with <http://hashify.me>, it is a very useful
Markdown previewer.
